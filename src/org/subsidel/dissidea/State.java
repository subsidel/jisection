/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.subsidel.dissidea;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

/**
 *
 * @author Joel
 */
public class State {
    //shhh I'm being quick :P
    public  GL gl;
    public  GLAutoDrawable my_canvas;
    public  static int[][][] data;
    public  static int xSize;
    public  static int ySize;
    public  static int zSize;
    public  static int[] xyzSizes = new int[3];
    public  static final int  X_SPLICE = 0;
    public  static final int Y_SPLICE = 1;
    public  static final int Z_SPLICE = 2;
    public  static int spliceAxis = 0;
    public  static int row = 0;
    public static boolean changeFile = false;
    public  JFileChooser fc;
    public static JTextField spliceAndDepth = new JTextField(15);
    
    
    
        /**
     * moves the row count on to the next layer of the data set
     */
    public static boolean nextLayer() {
        boolean ret = false;
            if (row != xyzSizes[spliceAxis] - 1) {
                row += 1;
                ret = true;
            }
        return ret;
    }

    /**
     * moves the row count on to the previous layer of the data set
     */
    public static boolean prevLayer() {
        if (row != 0) {
            row -= 1;
            return true;
        }
        return false;
    }
    
    
}
