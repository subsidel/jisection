/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.subsidel.dissidea;

import com.leapmotion.leap.Controller;
import com.sun.opengl.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.media.opengl.*;
import javax.swing.*;

public class Slicer extends JFrame {

    static Animator loop = null;
    static State state;
    static String defaultFile = "/home/joel/Graphics_Data/skull.raw";
    public static String file;
    static Slicer app;
    static SlicerListener renderer;
    static LeapListener leapListen;
    public static JMenuBar menuBar;
    public static JMenu fileMenu;
    public static JMenuItem loadFile;
    public static JMenu viewMenu;
    public static JMenuItem changeToX;
    public static JMenuItem changeToY;
    public static JMenuItem changeToZ;
    public static JMenuItem skipToRow;
    Controller controller = new Controller();

    public static void main(String[] args) {
       // System.load("/home/joel/LeapDevKit/LeapDeveloperKit/LeapSDK/lib/x64/LeapJava.so");
        System.loadLibrary("libLeapJava");
        if (args != null) {
            file = defaultFile;
        } else {
            file = args[0];
        }
        
        state = new State();
        leapListen = new LeapListener(state);
        
        
        app = new Slicer();
        // show what we've done
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                app.setVisible(true);
            }
        });

        // start the animator
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                loop.start();
            }
        });
    }

    public Slicer() {
        super("Dissertation idea");
        // kill the process when the JFrame is closed
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new Slicer.shutDownWindow());
        
        renderer = new SlicerListener(state);
        GLCanvas my_drawable = new GLCanvas();
        
        my_drawable.addGLEventListener(renderer);
        loop = new Animator(my_drawable);
        controller.addListener(leapListen);
        getContentPane().add(my_drawable, BorderLayout.CENTER);
        setSize(700, 500);
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        setJMenuBar(getMyMenuBar());
        getContentPane().add(state.spliceAndDepth, BorderLayout.BEFORE_FIRST_LINE);
        centreWindow(this);

    }

    /**
     *
     * @param frame
     */
    public void centreWindow(Component frame) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = frame.getSize();

        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }
        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }

        frame.setLocation((screenSize.width - frameSize.width) >> 1,
                (screenSize.height - frameSize.height) >> 1);
    }
    
//******************************
    private static JMenuBar getMyMenuBar() {
        menuBar = new JMenuBar();
        fileMenu = new JMenu("File");
        loadFile = new JMenuItem("Load");
        FileSelector fileChooser = new FileSelector();
        loadFile.addActionListener(fileChooser);
        
        viewMenu = new JMenu("View");
        
        changeToX = new JMenuItem("Splice x");
        changeToX.addActionListener(renderer);
        changeToY = new JMenuItem("Splice y");
        changeToY.addActionListener(renderer);
        changeToZ = new JMenuItem("Splice z");
        changeToZ.addActionListener(renderer);
        skipToRow = new JMenuItem("Skip to row");
        skipToRow.addActionListener(renderer);
        
        fileMenu.add(loadFile);
        viewMenu.add(changeToX);
        viewMenu.add(changeToY);
        viewMenu.add(changeToZ);
        viewMenu.add(skipToRow);

        menuBar.add(fileMenu);
        menuBar.add(viewMenu);
        return menuBar;
    }
//******************************
    
    public class shutDownWindow extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            loop.stop();
        }
    }
}