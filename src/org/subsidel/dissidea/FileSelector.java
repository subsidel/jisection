package org.subsidel.dissidea;

import java.awt.event.*;
import java.io.*;
import javax.swing.*;

/**
 *
 * @author joel
 */
public class FileSelector extends JPanel
        implements ActionListener {

    JFileChooser fc;

    public FileSelector() {
        //Create a file chooser
        fc = new JFileChooser();
    }

    public void actionPerformed(ActionEvent e) {

        //Handle open button action.
        if (e.getSource() == Slicer.loadFile) {
            int returnVal = fc.showOpenDialog(FileSelector.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                Slicer.file = file.getPath();
                SlicerListener.state.changeFile = true;
            }
        }
    }
}