package org.subsidel.dissidea;

import binaryreader.BinaryReader;
import java.io.IOException;
import java.awt.event.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.*;
import javax.media.opengl.glu.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class SlicerListener
        implements GLEventListener,
        KeyListener, 
        MouseListener,
        MouseMotionListener, 
        ActionListener 
{

    public static org.subsidel.dissidea.State state;

    public SlicerListener() {
    }

    public SlicerListener(org.subsidel.dissidea.State state) {
        SlicerListener.state = state;
    }

    /**
     * Take care of initialisation here.
     */
    public void init(GLAutoDrawable gld) {

        GLU glu = new GLU();
        state.gl = gld.getGL();

        loadFile();

        gld.addKeyListener(this);   // connect keyboard
        gld.addMouseListener(this); // connect mouse
        gld.addMouseMotionListener(this); // connect mouse

        state.my_canvas = (GLAutoDrawable) gld; // save Drawable

        state.gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        state.gl.glViewport(0, 0, state.xSize, state.ySize);
        state.gl.glMatrixMode(GL.GL_PROJECTION);
        state.gl.glLoadIdentity();
        glu.gluOrtho2D(0.0, state.xSize, 0.0, state.ySize);
    }

    private void loadFile() {
        State.row = 0; //avoid out of bounds
        //assume 1:1:1 ratio of data
        State.ySize = State.zSize = State.xSize = guessFileSize(new File(Slicer.file));
        State.xyzSizes = new int[] {State.xSize ,State.ySize, State.zSize};
        try {
            org.subsidel.dissidea.State.data =
                    BinaryReader.loadData(Slicer.file,
                    org.subsidel.dissidea.State.xSize,
                    org.subsidel.dissidea.State.ySize,
                    org.subsidel.dissidea.State.zSize);

        } catch (IOException ex) {
            Logger.getLogger(Slicer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void display(GLAutoDrawable drawable) {
        state.gl = drawable.getGL();

        state.gl.glClear(GL.GL_COLOR_BUFFER_BIT);

        if (org.subsidel.dissidea.State.changeFile == true) {
            loadFile();
            org.subsidel.dissidea.State.changeFile = false;
        }
        if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.X_SPLICE) {
            xSplice(state.gl, org.subsidel.dissidea.State.row);
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Y_SPLICE) {
            ySplice(state.gl, org.subsidel.dissidea.State.row);
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Z_SPLICE) {
            zSplice(state.gl, org.subsidel.dissidea.State.row);
        }

        org.subsidel.dissidea.State.spliceAndDepth.setText("Splice Axis: "
                + org.subsidel.dissidea.State.spliceAxis
                + " Depth: " + org.subsidel.dissidea.State.row);
        state.gl.glFlush();
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        GLU glu = new GLU();
        state.gl.glViewport(0, 0, org.subsidel.dissidea.State.xSize,
                org.subsidel.dissidea.State.ySize);
        state.gl.glMatrixMode(GL.GL_PROJECTION);
        state.gl.glLoadIdentity();
        glu.gluOrtho2D(0.0, drawable.getWidth(), 0.0, drawable.getHeight());
    }

    /**
     * Invoked when a key has been typed. See the class description for KeyEvent
     * for a definition of a key typed event.
     *
     * @param k The KeyEvent.
     */
    public void keyTyped(KeyEvent k) {
        if (k.getKeyChar() == 'x') {
            org.subsidel.dissidea.State.row = 0;
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.X_SPLICE;
        } else if (k.getKeyChar() == 'y') {
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Y_SPLICE;
            org.subsidel.dissidea.State.row = 0;
        } else if (k.getKeyChar() == 'z') {
            org.subsidel.dissidea.State.row = 0;
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Z_SPLICE;
        } else if (k.getKeyChar() == '+') {
            org.subsidel.dissidea.State.nextLayer();
        } else if (k.getKeyChar() == '-') {
            org.subsidel.dissidea.State.prevLayer();
        }
    }

    /**
     * Invoked when a key has been released.
     *
     * @param e The KeyEvent.
     */
    public void keyReleased(KeyEvent k) {
    }

    // Invoked when a key has been pressed:
    public void keyPressed(KeyEvent k) {
    }

    public void mouseEntered(MouseEvent m) {
    }

    public void mouseExited(MouseEvent m) {
    }

    /**
     * listen for mouse button pressed
     *
     * @param m
     */
    public void mousePressed(MouseEvent m) {
        if (m.getButton() == MouseEvent.BUTTON1) {
            org.subsidel.dissidea.State.nextLayer();
        } else if (m.getButton() == MouseEvent.BUTTON3) {
            org.subsidel.dissidea.State.prevLayer();
        }
    }

    public void mouseReleased(MouseEvent m) {
    }

    public void mouseClicked(MouseEvent m) {
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    /**
     * moves the frame of reference to the X axis y goes along, z goes up
     *
     * @param gl
     * @param x
     */
    private static void xSplice(GL gl, int x) {
        gl.glPointSize(10);
        gl.glBegin(GL.GL_POINTS);

        for (int y = 0; y < org.subsidel.dissidea.State.ySize; y++) {
            for (int z = 0; z < org.subsidel.dissidea.State.zSize; z++) {
                double c = org.subsidel.dissidea.State.data[x][y][z] / 100.0 + 0.5;

                gl.glColor3d(c, c, c);
                gl.glVertex2i(y, z);
                //System.out.println(data[x][y][z]);
            }
        }
    }

    /**
     * moves the frame of reference to the Y axis x goes along, z goes up
     *
     * @param gl
     * @param x
     */
    private static void ySplice(GL gl, int y) {
        gl.glPointSize(10);
        gl.glBegin(GL.GL_POINTS);

        for (int x = 0; x < org.subsidel.dissidea.State.xyzSizes[org.subsidel.dissidea.State.X_SPLICE]; x++) {
            for (int z = 0; z < org.subsidel.dissidea.State.xyzSizes[org.subsidel.dissidea.State.Z_SPLICE]; z++) {
                double c = org.subsidel.dissidea.State.data[x][y][z] / 100.0 + 0.5;

                gl.glColor3d(c, c, c);
                gl.glVertex2i(x, z);
                //System.out.println(data[x][y][z]);
            }
        }
    }
    

    /**
     * moves the frame of reference to the Z axis x goes along, y goes up
     *
     * @param gl
     * @param x
     */
    private static void zSplice(GL gl, int z) {
        gl.glPointSize(10);
        gl.glBegin(GL.GL_POINTS);

        for (int x = 0; x < org.subsidel.dissidea.State.xSize; x++) {
            for (int y = 0; y < org.subsidel.dissidea.State.ySize; y++) {
                double c = org.subsidel.dissidea.State.data[x][y][z] / 100.0 + 0.5;

                gl.glColor3d(c, c, c);
                gl.glVertex2i(x, y);
                //System.out.println(data[x][y][z]);
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Slicer.changeToX) {
            org.subsidel.dissidea.State.row = 0;
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.X_SPLICE;
        }
        if (e.getSource() == Slicer.changeToY) {
            org.subsidel.dissidea.State.row = 0;
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Y_SPLICE;
        }
        if (e.getSource() == Slicer.changeToZ) {
            org.subsidel.dissidea.State.row = 0;
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Z_SPLICE;
        }
        if (e.getSource() == Slicer.skipToRow) {
            org.subsidel.dissidea.State.row = askRows();
        }

    }

    public int askRows() {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        Integer value = new Integer(org.subsidel.dissidea.State.row);
        Integer min = new Integer(0);
        Integer max;
        if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.X_SPLICE) {
            max = new Integer(org.subsidel.dissidea.State.xSize);
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Y_SPLICE) {
            max = new Integer(org.subsidel.dissidea.State.ySize);
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Z_SPLICE) {
            max = new Integer(org.subsidel.dissidea.State.zSize);
        } else {
            max = org.subsidel.dissidea.State.row;
        }
        Integer step = new Integer(1);
        SpinnerNumberModel model = new SpinnerNumberModel(value, min, max, step);
        JSpinner spinner = new JSpinner(model);
        panel.add(new JLabel("Row: "));
        panel.add(spinner);
        int input = JOptionPane.showConfirmDialog(frame,
                panel,
                "Enter A Row",
                JOptionPane.OK_OPTION,
                JOptionPane.CANCEL_OPTION);
        int output = org.subsidel.dissidea.State.row;
        if (input == JOptionPane.OK_OPTION) {
            // OK was pressed
            output = (Integer) spinner.getValue();
        }
        return output;
    }

    public static int guessFileSize(File file) {
        double sideGuess = Math.pow(file.length(), 1.0 / 3);
        int dimentionGuess = (int) -Math.floor(-sideGuess);
        return dimentionGuess;
    }
}