/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.subsidel.dissidea;

import com.leapmotion.leap.*;
import com.leapmotion.leap.Gesture.State;

class LeapListener extends Listener {

    private org.subsidel.dissidea.State state;
    private LeapNumbers numbers = new LeapNumbers();

    public LeapListener(org.subsidel.dissidea.State state) {
        this.state = state;
    }

    @Override
    public void onInit(Controller controller) {
        System.out.println("Initialized");
    }

    @Override
    public void onConnect(Controller controller) {
        System.out.println("Connected");
        controller.enableGesture(Gesture.Type.TYPE_SWIPE);
        controller.enableGesture(Gesture.Type.TYPE_CIRCLE);
        controller.enableGesture(Gesture.Type.TYPE_SCREEN_TAP);
        controller.enableGesture(Gesture.Type.TYPE_KEY_TAP);
    }

    @Override
    public void onDisconnect(Controller controller) {
        //Note: not dispatched when running in a debugger.
        System.out.println("Disconnected");
    }

    @Override
    public void onExit(Controller controller) {
        System.out.println("Exited");
    }

    public void handleSwipe(SwipeGesture swipe) {

        if (swipe.state().equals(State.STATE_START) && !numbers.beginSwipe) {
            numbers.beginSwipe = true;
            numbers.endSwipe = false;

            numbers.startX = swipe.startPosition().getX();
            numbers.startY = swipe.startPosition().getY();
            numbers.startZ = swipe.startPosition().getZ();
        } else if (swipe.state().equals(State.STATE_STOP) && !numbers.endSwipe) {
            numbers.endX = swipe.position().getX();
            numbers.endY = swipe.position().getY();
            numbers.endZ = swipe.position().getZ();
        }

        String xDirection = "";
        String yDirection = "";
        String zDirection = "";

        numbers.swipedDirection = 0;
        if (Math.abs(numbers.xDifference()) > numbers.xSensitivity) {
            if (numbers.xDifference() > 0) {
                xDirection = "left";
                numbers.swipedDirection += numbers.LEFT;
                System.out.println("left-swipe");
            } else {
                xDirection = "right";
                numbers.swipedDirection += numbers.RIGHT;
                System.out.println("right-swipe");
            }
        }

        if (Math.abs(numbers.yDifference()) > numbers.ySensitivity) {
            if (numbers.yDifference() > 0) {
                yDirection = "down";
                System.out.println("down-swipe");
                numbers.swipedDirection += numbers.DOWN;
            } else {
                yDirection = "up";
                System.out.println("up-swipe");
                numbers.swipedDirection += numbers.UP;
            }
        }

        if (Math.abs(numbers.zDifference()) > numbers.zSensitivity) {

            if (numbers.zDifference() > 0) {
                zDirection = "forward ";
                numbers.swipedDirection += numbers.FORWARD;
                //playSelected();
            } else {
                zDirection = "back ";
                numbers.swipedDirection += numbers.BACK;
            }
        }

        if (swipe.state().equals(SwipeGesture.State.STATE_STOP) && !numbers.endSwipe) {
            numbers.endSwipe = true;
            numbers.beginSwipe = false;
            String swipeDirections = xDirection + yDirection + zDirection;
  
        }

        doSwipe(numbers.swipedDirection, (int)(Math.abs(numbers.zDifference())));

        //reset positions
        numbers.startX = 0;
        numbers.endX = 0;
        numbers.startY = 0;
        numbers.endY = 0;
        numbers.startZ = 0;
        numbers.endZ = 0;
    }

    public void doSwipe(int direction, int repeat){
        try{
            if (numbers.swipedDirection == numbers.FORWARD) {
                synchronized(this){
                for(int i = 0; i <= repeat ;i++){
                    if(org.subsidel.dissidea.State.nextLayer()) {
                        this.wait(5);
                    }
                }
            }

            } else if (numbers.swipedDirection == numbers.BACK) {
                synchronized(this){
                    for(int i = 0; i <= repeat;i++){
                        if(org.subsidel.dissidea.State.prevLayer()){
                            this.wait(5);
                        }
                    }
                }
                
            } else if (numbers.swipedDirection == numbers.LEFT) {
                rotateLeft();
            } else if (numbers.swipedDirection == numbers.RIGHT) {
                rotateRight();
            }
        }catch(Exception ex){
           System.err.println(ex.toString());
        }
    }

    public void rotateLeft() {
        if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Y_SPLICE) {
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Z_SPLICE;
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Z_SPLICE) {
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.X_SPLICE;
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.X_SPLICE) {
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Y_SPLICE;
        }
    }

    public void rotateRight() {
        if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Y_SPLICE) {
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.X_SPLICE;
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.Z_SPLICE) {
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Y_SPLICE;
        } else if (org.subsidel.dissidea.State.spliceAxis == org.subsidel.dissidea.State.X_SPLICE) {
            org.subsidel.dissidea.State.spliceAxis = org.subsidel.dissidea.State.Z_SPLICE;
        }
    }

    public void doScreenTap(ScreenTapGesture screenTap) {
    }

    public void doKeyTap(KeyTapGesture keyTap) {
    }

    @Override
    public void onFrame(Controller controller) {
        // Get the most recent frame and report some basic information
        Frame frame = controller.frame();
      

        if (!frame.hands().isEmpty()) {
            // Get the first hand
            Hand hand = frame.hands().get(0);

            // Check if the hand has any fingers
            FingerList fingers = hand.fingers();
            if (!fingers.isEmpty()) {
                // Calculate the hand's average finger tip position
                Vector avgPos = Vector.zero();
                for (Finger finger : fingers) {
                    avgPos = avgPos.plus(finger.tipPosition());
                }
                avgPos = avgPos.divide(fingers.count());
               
            }
            // Get the hand's normal vector and direction
            Vector normal = hand.palmNormal();
            Vector direction = hand.direction();
          
        }

        GestureList gestures = frame.gestures();
        for (int i = 0; i < gestures.count(); i++) {
            Gesture gesture = gestures.get(i);

            switch (gesture.type()) {
                case TYPE_CIRCLE:
                    CircleGesture circle = new CircleGesture(gesture);

                    // Calculate clock direction using the angle between circle normal and pointable
                    String clockwiseness;
                    if (circle.pointable().direction().angleTo(circle.normal()) <= Math.PI / 4) {
                        // Clockwise if angle is less than 90 degrees
                        clockwiseness = "clockwise";
                    } else {
                        clockwiseness = "counterclockwise";
                    }

                    // Calculate angle swept since last frame
                    double sweptAngle = 0;
                    if (circle.state() != State.STATE_START) {
                        CircleGesture previousUpdate = new CircleGesture(controller.frame(1).gesture(circle.id()));
                        sweptAngle = (circle.progress() - previousUpdate.progress()) * 2 * Math.PI;
                    }

                    System.out.println("Circle id: " + circle.id()
                            + ", " + circle.state()
                            + ", progress: " + circle.progress()
                            + ", radius: " + circle.radius()
                            + ", angle: " + Math.toDegrees(sweptAngle)
                            + ", " + clockwiseness);
                    break;
                case TYPE_SWIPE:
                    SwipeGesture swipe = new SwipeGesture(gesture);
                    handleSwipe(swipe);

                    break;
                case TYPE_SCREEN_TAP:
                    ScreenTapGesture screenTap = new ScreenTapGesture(gesture);
                    doScreenTap(screenTap);
                    break;
                case TYPE_KEY_TAP:
                    KeyTapGesture keyTap = new KeyTapGesture(gesture);
                    doKeyTap(keyTap);
                    break;
                default:
                    System.out.println("Unknown gesture type.");
                    break;
            }
        }

        if (!frame.hands().isEmpty() || !gestures.isEmpty()) {
        }
    }
}