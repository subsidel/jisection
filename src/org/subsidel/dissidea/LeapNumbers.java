/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.subsidel.dissidea;

/**
 *
 * @author Joel
 */
public class LeapNumbers {
        public final int CLOCKWISE = 1;
	public final int ANTICLOCKWISE = 2;
	public final int LEFT_DOWN_FORWARD = 111;
	public final int LEFT_DOWN_BACK = 211;
	public final int LEFT_UP_FORWARD = 121;
	public final int LEFT_UP_BACK = 221;
	public final int RIGHT_DOWN_FORWARD = 112;
	public final int RIGHT_DOWN_BACK = 212;
	public final int RIGHT_UP_FORWARD = 122;
	public final int RIGHT_UP_BACK = 222;
	public final int LEFT_DOWN = 11;
	public final int LEFT_UP = 21;
	public final int LEFT_FORWARD = 101;
	public final int LEFT_BACK = 201;
	public final int RIGHT_DOWN = 12;
	public final int RIGHT_UP = 22;
	public final int RIGHT_FORWARD = 102;
	public final int RIGHT_BACK = 202;
	public final int LEFT = 1;
	public final int RIGHT = 2;
	public final int DOWN = 10;
	public final int UP = 20;
	public final int FORWARD = 100;
	public final int BACK = 200;
	public final int clockwiseness = 1;
	public boolean canSeeHand = false;
	public int swipeDirections;
	public int circles = 1;
	public int circleDirection = 0;
	public String previousClockwiseness = "";
	public boolean beginSwipe = false;
	public boolean endSwipe = false;
	public double startX;
	public double endX;
	public double startY;
	public double endY;
	public double startZ;
	public double endZ;
	public double frm;
	public double xSensitivity = 90;
	public double ySensitivity = 170;
	public double zSensitivity = 50;
	public int swipedDirection = 0;

	public double xDifference(){
		return this.startX - this.endX;
	}
	public double yDifference() {
		return this.startY - this.endY;
	}
	public double zDifference() {
		return this.startZ - this.endZ;
	}
    
}
